# Fun Hog Sub-250 Gram Frame V3 "Naked"

WIP/PLACEHOLDER The natural evolution of the Fun Hog providing support for "naked" type camera mounting, such as the Naked Thumb.

# License

__Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)__

https://creativecommons.org/licenses/by-nc/4.0/

See <a href="LICENSE.md">LICENSE</a>.

## Summary

* __Attribution__ — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* __NonCommercial__ — You may not use the material for commercial purposes.

<img src="lib/all-designs.png"/>


## Commercial Licensing

All designs are available for commercial licensing, please contact FunHog.me us directly.
